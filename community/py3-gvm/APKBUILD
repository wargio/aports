# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=py3-gvm
_pkgname=${pkgname/py3/python}
pkgver=22.9.1
pkgrel=0
pkgdesc="Greenbone Vulnerability Management Python Library "
url="https://github.com/greenbone/python-gvm"
arch="noarch"
license="GPL-3.0-or-later"
makedepends="py3-gpep517 py3-installer py3-poetry-core"
checkdepends="py3-defusedxml py3-pytest py3-lxml py3-paramiko"
source="$pkgname-$pkgver.tar.gz::https://github.com/greenbone/$_pkgname/archive/v${pkgver/_/.}.tar.gz"
builddir="$srcdir/$_pkgname-${pkgver/_/.}"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	# hangs
	testenv/bin/python3 -m pytest \
		--deselect=tests/connections/test_ssh_connection.py
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
97de30e09ead0a949d09077c64d4dd18aa8595d5363fa0aed67349eda7f22e904c540e4ca75656af80c38e03c7a3f84fcde6153d68584c9f89c732a850d6aa0c  py3-gvm-22.9.1.tar.gz
"
