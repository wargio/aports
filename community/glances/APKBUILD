# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=glances
pkgver=3.3.1
pkgrel=0
pkgdesc="CLI curses based monitoring tool"
url="https://nicolargo.github.io/glances/"
arch="noarch"
license="LGPL-3.0-or-later"
depends="py3-future py3-psutil py3-bottle py3-snmp py3-batinfo docker-py py3-defusedxml"
makedepends="py3-setuptools"
subpackages="$pkgname-doc"
source="glances-$pkgver.tar.gz::https://github.com/nicolargo/glances/archive/v$pkgver.tar.gz"
options="!check"  # tests fail on builder infra due to env. limitations

build() {
	python3 setup.py build
}

check() {
	python3 unitest.py
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	# Remove images and HTML doc depending on those
	rm -rf "$pkgdir"/usr/share/doc/$pkgname/$pkgname-doc.html \
		"$pkgdir"/usr/share/doc/$pkgname/images
}

sha512sums="
4c75000552d7736a010edd9f5ca8afc9ff2a48efc5603d363bf3d3d396980a345480c0afd16b3822f1ae9519584bc076be37390bc2c5477285bda15a93d82bf8  glances-3.3.1.tar.gz
"
