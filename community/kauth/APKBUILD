# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kauth
pkgver=5.102.0
pkgrel=0
pkgdesc="Abstraction to system policy and authentication features"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	kcoreaddons-dev
	polkit-qt-1-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kauth-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# KAuthHelperTest hangs
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E '(KAuthHelperTest)'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
33a81814043056eca58d342b8cfc1e3fabcdc041858c6d927580039df0529cf51061178e49754e1a523cbcd005c1a4f44fe9ecaa1a5b40a96dc698351d2cd3ba  kauth-5.102.0.tar.xz
"
