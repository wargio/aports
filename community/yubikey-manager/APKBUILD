# Contributor: Daniel Everett <deverett@gmail.com>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=yubikey-manager
pkgver=4.0.9
pkgrel=4
pkgdesc="Python library and command line tool for configuring any YubiKey over all USB interfaces"
url="https://developers.yubico.com/yubikey-manager/"
arch="noarch"
license="BSD-2-Clause"
depends="
	ccid
	pcsc-lite
	pcsc-lite-libs
	py3-click
	py3-cryptography
	py3-fido2
	py3-openssl
	py3-pyscard
	py3-ykman
	python3
	ykpers
	yubico-c
	"
makedepends="
	py3-poetry-core
	py3-gpep517
	"
checkdepends="py3-pytest py3-makefun"
subpackages="py3-ykman"
source="https://github.com/Yubico/yubikey-manager/releases/download/$pkgver/yubikey-manager-$pkgver.tar.gz"
builddir="$srcdir/yubikey-manager-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

ykman() {
	depends=
	amove usr/lib
}

sha512sums="
bbffbdf9437d0f2de0b7713fe577a342c8da95e9d9512cb5118374b63637dbde6ee95e516dbb47bbea3af5b2e5edd495882c7e4adcbec1fd3eb750471a50add5  yubikey-manager-4.0.9.tar.gz
"
