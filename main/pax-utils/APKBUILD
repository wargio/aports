# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=pax-utils
pkgver=1.3.6
pkgrel=0
pkgdesc="ELF related utils for ELF 32/64 binaries"
url="https://wiki.gentoo.org/wiki/Hardened/PaX_Utilities"
arch="all"
license="GPL-2.0-only"
depends="scanelf"
makedepends_build="meson"
makedepends_host="linux-headers libcap-dev"
checkdepends="bash python3 py3-elftools"
[ -n "$BOOTSTRAP" ] && options="$options !check" # prevent python dependency
source="https://dev.gentoo.org/~sam/distfiles/app-misc/pax-utils/pax-utils-$pkgver.tar.xz"
subpackages="scanelf:_scanelf lddtreepax:_lddtreepax:noarch"

build() {
	abuild-meson \
		-Dlddtree_implementation=sh \
		-Duse_seccomp=false \
		. output
	meson compile -C output
}

check() {
	meson test --print-errorlogs --no-rebuild -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output

	# Don't conflict with main/lddtree
	mv "$pkgdir"/usr/bin/lddtree "$pkgdir"/usr/bin/lddtreepax
}

_scanelf() {
	depends=""
	pkgdesc="Scan ELF binaries for stuff"
	replaces="pax-utils"

	amove usr/bin/scanelf
}

_lddtreepax() {
	depends="python3 py3-elftools"
	pkgdesc="Read and package ELF dependency trees"

	amove usr/bin/lddtreepax
}

sha512sums="
94d6bdcac0109cc7218fc523594100b4bb5877e0709f5443903ae8aca96e8f3f45cf47b28b57ac146caf6713ad7895a6f67adf4364d9a14986c8b7e0399f3865  pax-utils-1.3.6.tar.xz
"
