# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=py3-sphinx-autodoc-typehints
_pyname=sphinx-autodoc-typehints
pkgver=1.21.2
pkgrel=0
pkgdesc="Type hints support for the Sphinx autodoc extension"
url="https://github.com/tox-dev/sphinx-autodoc-typehints"
arch="noarch"
license="MIT"
options="net"
depends="python3 py3-sphinx py3-typing-extensions"
makedepends="py3-gpep517 py3-installer py3-hatchling py3-hatch-vcs"
checkdepends="py3-pytest py3-sphobjinv py3-nptyping"
source="$_pyname-$pkgver.tar.gz::https://github.com/tox-dev/sphinx-autodoc-typehints/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir/$_pyname-$pkgver"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --system-site-packages test-env
	test-env/bin/python3 -m installer dist/sphinx_autodoc_typehints*.whl
	test-env/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/sphinx_autodoc_typehints*.whl
}

sha512sums="
d4b642659172f195a5fe44130df5a4383e876b48e2a8d9252303cc8696febeb7e34416643b89e1ac6f5af8d65d7834f3c8aad8349b49c734c5f3b44399378539  sphinx-autodoc-typehints-1.21.2.tar.gz
"
