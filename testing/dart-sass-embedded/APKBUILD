# Contributor: Nulo <nulo@sutty.coop.ar>
# Maintainer: Nulo <nulo@sutty.coop.ar>
pkgname=dart-sass-embedded
pkgver=1.57.1
pkgrel=0
pkgdesc="The primary implementation of Sass"
url="https://sass-lang.com/dart-sass"
arch="x86_64"	# dart
license="MIT"
# bundles dart-sass
depends="dart"
makedepends="dart-sdk dart-dev protobuf"
options="net"
_protocol_ver=1.2.0
source="
	https://github.com/sass/dart-sass-embedded/archive/refs/tags/$pkgver/dart-sass-embedded-$pkgver.tar.gz
	embedded-protocol-$_protocol_ver.tar.gz::https://github.com/sass/embedded-protocol/archive/refs/tags/$_protocol_ver.tar.gz
	add-shebang.patch

	dart-sass-embedded
	"
builddir="$srcdir/dart-sass-embedded-$pkgver/"

prepare() {
	default_prepare

	mkdir build/
	mv "$srcdir/embedded-protocol-$_protocol_ver" build/embedded-protocol

	dart pub get
}

build() {
	UPDATE_SASS_PROTOCOL=false \
		dart run grinder protobuf
	dart run grinder pkg-compile-snapshot
}

check() {
	# sanity
	dart run build/dart-sass-embedded.snapshot --version
}

package() {
	install -Dm755 "$srcdir"/dart-sass-embedded "$pkgdir"/usr/bin/dart-sass-embedded
	install -Dm644 build/dart-sass-embedded.snapshot "$pkgdir"/usr/lib/dart-sass-embedded/dart-sass-embedded.snapshot
}

sha512sums="
5428cf7ea69f63529efef0e5c3d4c06def7ac39d676f010287e8583d898266ec1b432e4e08e968cbb3630f7077819dce49a8904e4a9dbebf59238ecdd3d0c2d6  dart-sass-embedded-1.57.1.tar.gz
5d836af58501b1664ac621a544d00ffdbc8e2d7ace3748e24811ecdd5b6b1c6e95d0c130386d4b5271a1265f02b262533db463d6bb70645bf732918821aa6667  embedded-protocol-1.2.0.tar.gz
b5824d513ab2f8753476c88e782bba993c9f0a3fb52ee1a6608eb75547854a4ad8fcb8767bf8968d2507412b60c22a4659a6a769f39c948e4f34f8f0ddd30140  add-shebang.patch
b90f571b035a0d3f1d641556db0c2a0194d7a4f08fa936d46182fff6feb92b29a9367f96d3e186f037b4a251984c06fb31952ec451376e7488b9c50d68f2c61c  dart-sass-embedded
"
